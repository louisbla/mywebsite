<?php
/*
Template Name: Page resultats
 */

    get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <?php
            // Start the loop.
            while ( have_posts() ) : the_post();

                //Affiche le graphique en barres.
                //echo do_shortcode( '[wp_charts title="barchart" class="graphResults" type="bar" align="alignleft" margin="5px 20px" datasets="40,100,50,35,60" labels="Employés,Traitements,Clients,Dispositifs,Partenaires" width="40%"scaleOverride="true" scaleSteps="5" scaleStepWidth="20" scaleStartValue="0" scaleFontSize="20"]');

                echo $_COOKIE["cf7msm_posted_data"];

                echo 'score client : ' . scoreClient();

                // Include the page content template.
                get_template_part( 'template-parts/content-page', 'page' );

                // If comments are open or we have at least one comment, load up the comment template.
                if ( comments_open() || get_comments_number() ) {
                    comments_template();
                }

                // End of the loop.
            endwhile;
            ?>

        </main><!-- .site-main -->

        <?php get_sidebar( 'content-bottom' ); ?>

    </div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
?>


<?php
function scoreClient()
{
    $score = 0;

    if(do_shortcode('[multiform "client-1"]') == 'Vrai')
        $score++;
    if(do_shortcode('[multiform "client-2"]') == 'Faux')
        $score++;
    if(do_shortcode('[multiform "client-3"]') == 'Faux')
        $score++;
    if(do_shortcode('[multiform "client-4"]') == 'Faux')
        $score++;
    if(do_shortcode('[multiform "client-5"]') == 'Faux')
        $score++;
    if(do_shortcode('[multiform "client-6"]') == 'Faux')
        $score++;
    if(do_shortcode('[multiform "client-7"]') == 'Vrai')
        $score++;

    return $score;
}

function scoreSalarie()
{
    $score = 0;

    if(do_shortcode('[multiform "salarie-1"]') == 'Faux')
        $score++;
    if(do_shortcode('[multiform "salarie-2"]') == 'Faux')
        $score++;
    if(do_shortcode('[multiform "salarie-3"]') == 'Faux')
        $score++;
    if(do_shortcode('[multiform "salarie-4"]') == 'Faux')
        $score++;
    if(do_shortcode('[multiform "salarie-5"]') == 'Faux')
        $score++;
    if(do_shortcode('[multiform "salarie-6"]') == 'Faux')
        $score++;
    if(do_shortcode('[multiform "salarie-7"]') == 'Vrai')
        $score++;

    return $score;
}

function scorePartenaire()
{
    $score = 0;

    if(do_shortcode('[multiform "partenaire-1"]') == 'Vrai')
        $score++;
    if(do_shortcode('[multiform "partenaire-2"]') == 'Faux')
        $score++;
    if(do_shortcode('[multiform "partenaire-3"]') == 'Vrai')
        $score++;
    if(do_shortcode('[multiform "partenaire-4"]') == 'Vrai')
        $score++;
    if(do_shortcode('[multiform "partenaire-5"]') == 'Vrai')
        $score++;
    if(do_shortcode('[multiform "partenaire-6"]') == 'Vrai')
        $score++;

    return $score;
}

function scoreProcessus()
{
    $score = 0;

    if(do_shortcode('[multiform "processus-1"]') == 'Vrai')
        $score++;
    if(do_shortcode('[multiform "processus-2"]') == 'Faux')
        $score++;
    if(do_shortcode('[multiform "processus-3"]') == 'Vrai')
        $score++;
    if(do_shortcode('[multiform "processus-4"]') == 'Faux')
        $score++;
    if(do_shortcode('[multiform "processus-5"]') == 'Vrai')
        $score++;

    return $score;
}

?>