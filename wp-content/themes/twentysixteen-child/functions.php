<?php
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

}

// Change le lien par defaut de la page de connexion (wordpress.org)
function wpc_url_login(){
    return "http://localhost/myWebsite/"; // votre URL ici
}
add_filter('login_headerurl', 'wpc_url_login');