<?php
/**
 * Created by PhpStorm.
 * User: HP_LOUIS
 * Date: 15/06/2018
 * Time: 11:50
 * Plugin Name: echoResultats
 * Description: Plugin perso - Retourne les résultats du formulaire RGPD
 */

function echoResultats(){

    return do_shortcode('[wp_charts title="barchart" class="graphResults" type="bar" align="alignleft" margin="5px 20px"
    datasets="' . scoreClient() . ',' . scoreSalarie() . ',' . scorePartenaire() . ',' . scoreProcessus() . ',' . scoreTraitement() . '" labels="Clients,Salaries,Partenaires,Processus,Traitement" width="40%" scaleOverride="true" scaleSteps="5" scaleStepWidth="20" scaleStartValue="0" scaleFontSize="20"]');
}
add_shortcode('echo_resultats','echoResultats');

function scoreTotal(){

    return (scoreClient() + scoreSalarie() + scorePartenaire() + scoreProcessus() + scoreTraitement())/5;
}
add_shortcode('score_total','scoreTotal');

function getValueOfTag($tag, $completeString){
    $index = strrpos($completeString, $tag);
    $startPosition = $index + strlen($tag) + 5;

    $value = substr($completeString, $startPosition, 4);

    return $value;
}

function scoreClient()
{
    $score = 0;

    if(getValueOfTag('client-1', $_COOKIE["cf7msm_posted_data"]) == 'Vrai')
        $score++;
    if(getValueOfTag('client-2', $_COOKIE["cf7msm_posted_data"]) == 'Faux')
        $score++;
    if(getValueOfTag('client-3', $_COOKIE["cf7msm_posted_data"]) == 'Faux')
        $score++;
    if(getValueOfTag('client-4', $_COOKIE["cf7msm_posted_data"]) == 'Faux')
        $score++;
    if(getValueOfTag('client-5', $_COOKIE["cf7msm_posted_data"]) == 'Faux')
        $score++;
    if(getValueOfTag('client-6', $_COOKIE["cf7msm_posted_data"]) == 'Faux')
        $score++;
    if(getValueOfTag('client-7', $_COOKIE["cf7msm_posted_data"]) == 'Vrai')
        $score++;

    return round($score/7*100);
}
function scoreSalarie()
{
    $score = 0;

    if(getValueOfTag('salarie-1', $_COOKIE["cf7msm_posted_data"]) == 'Faux')
        $score++;
    if(getValueOfTag('salarie-2', $_COOKIE["cf7msm_posted_data"]) == 'Faux')
        $score++;
    if(getValueOfTag('salarie-3', $_COOKIE["cf7msm_posted_data"]) == 'Faux')
        $score++;
    if(getValueOfTag('salarie-4', $_COOKIE["cf7msm_posted_data"]) == 'Faux')
        $score++;
    if(getValueOfTag('salarie-5', $_COOKIE["cf7msm_posted_data"]) == 'Faux')
        $score++;
    if(getValueOfTag('salarie-6', $_COOKIE["cf7msm_posted_data"]) == 'Faux')
        $score++;
    if(getValueOfTag('salarie-7', $_COOKIE["cf7msm_posted_data"]) == 'Vrai')
        $score++;

    return round($score/7*100);}

function scorePartenaire()
{
    $score = 0;

    if(getValueOfTag('partenaire-1', $_COOKIE["cf7msm_posted_data"]) == 'Vrai')
        $score++;
    if(getValueOfTag('partenaire-2', $_COOKIE["cf7msm_posted_data"]) == 'Faux')
        $score++;
    if(getValueOfTag('partenaire-3', $_COOKIE["cf7msm_posted_data"]) == 'Vrai')
        $score++;
    if(getValueOfTag('partenaire-4', $_COOKIE["cf7msm_posted_data"]) == 'Vrai')
        $score++;
    if(getValueOfTag('partenaire-5', $_COOKIE["cf7msm_posted_data"]) == 'Vrai')
        $score++;
    if(getValueOfTag('partenaire-6', $_COOKIE["cf7msm_posted_data"]) == 'Vrai')
        $score++;

    return round($score/6*100);
}

function scoreProcessus()
{
    $score = 0;

    if(getValueOfTag('processus-1', $_COOKIE["cf7msm_posted_data"]) == 'Vrai')
        $score++;
    if(getValueOfTag('processus-2', $_COOKIE["cf7msm_posted_data"]) == 'Faux')
        $score++;
    if(getValueOfTag('processus-3', $_COOKIE["cf7msm_posted_data"]) == 'Vrai')
        $score++;
    if(getValueOfTag('processus-4', $_COOKIE["cf7msm_posted_data"]) == 'Faux')
        $score++;
    if(getValueOfTag('processus-5', $_COOKIE["cf7msm_posted_data"]) == 'Vrai')
        $score++;

    return round($score/5*100);
}

function scoreTraitement()
{
    $score = 0;

    if(getValueOfTag('traitement-1', $_COOKIE["cf7msm_posted_data"]) == 'Faux')
        $score++;
    if(getValueOfTag('traitement-2', $_COOKIE["cf7msm_posted_data"]) == 'Vrai')
        $score++;
    if(getValueOfTag('traitement-3', $_COOKIE["cf7msm_posted_data"]) == 'Faux')
        $score++;
    if(getValueOfTag('traitement-4', $_COOKIE["cf7msm_posted_data"]) == 'Faux')
        $score++;
    if(getValueOfTag('traitement-5', $_COOKIE["cf7msm_posted_data"]) == 'Faux')
        $score++;
    if(getValueOfTag('traitement-6', $_COOKIE["cf7msm_posted_data"]) == 'Vrai')
        $score++;
    if(getValueOfTag('traitement-7', $_COOKIE["cf7msm_posted_data"]) == 'Vrai')
        $score++;
    if(getValueOfTag('traitement-8', $_COOKIE["cf7msm_posted_data"]) == 'Vrai')
        $score++;
    if(getValueOfTag('traitement-9', $_COOKIE["cf7msm_posted_data"]) == 'Faux')
        $score++;
    if(getValueOfTag('traitement-10', $_COOKIE["cf7msm_posted_data"]) == 'Vrai')
        $score++;
    if(getValueOfTag('traitement-11', $_COOKIE["cf7msm_posted_data"]) == 'Faux')
        $score++;

    return round($score/11*100);
}

