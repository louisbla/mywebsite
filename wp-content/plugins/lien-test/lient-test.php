<?php
/**
 * Created by PhpStorm.
 * User: HP_LOUIS
 * Date: 15/06/2018
 * Time: 11:50
 * Plugin Name: lienTest
 * Description: Plugin perso - Affiche un boutton qui redirige vers le test
 */

function lienTest(){
    $string = "<form method=\"POST\" action=\"".getBaseUrl()."faire-le-test/\"><input class=\"button-test\" type=\"submit\" name=\"Ok\" value=\"Testez vous en moins de 5 minutes !\" ></form>";
    return $string;
}
add_shortcode('lien_test','lienTest');

function lienTestCenter(){
    $string = "<form method=\"POST\" action=\"".getBaseUrl()."faire-le-test/\" style='margin-right: auto;margin-left:  auto;width:  20%;'><input class=\"button-test\" type=\"submit\" name=\"Ok\" value=\"Testez vous en moins de 5 minutes !\" ></form>";
    return $string;
}
add_shortcode('lien_test_center','lienTestCenter');


function getBaseUrl()
{
    // output: /myproject/index.php
    $currentPath = $_SERVER['PHP_SELF'];

    // output: Array ( [dirname] => /myproject [basename] => index.php [extension] => php [filename] => index )
    $pathInfo = pathinfo($currentPath);

    // output: localhost
    $hostName = $_SERVER['HTTP_HOST'];

    // output: http://
    $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https://'?'https://':'http://';

    // return: http://localhost/myproject/
    return $protocol.$hostName.$pathInfo['dirname']."/";
}